FROM texlive/texlive:latest

LABEL maintainer="Lionel Rigoux <lionel.rigoux@gmail.com>"

# version tag
ARG PANDEMICS_VERSION
ARG TARGETARCH

# install dependencies
# ==========================================================================
RUN ln -snf /usr/share/zoneinfo/Etc/UTC /etc/localtime \
    && echo "Etc/UTC" > /etc/timezone \
    && apt-get update -y -qq \
    && apt-get upgrade -y -qq

# basic tools
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y -qq \
    ca-certificates curl git gzip jq openssh-client xz-utils

# image processing    
RUN apt-get install -y -qq librsvg2-bin

# node
RUN curl -fsSL https://deb.nodesource.com/setup_21.x | bash - \
    && apt-get install -y -qq nodejs

# pandoc
RUN echo "installing pandoc for $TARGETARCH" \
  && PANDOC_URL=`curl https://api.github.com/repos/jgm/pandoc/releases/latest | jq -r ".assets | map(.browser_download_url) | .[]" | grep "$TARGETARCH.deb"` \
  && echo $PANDOC_URL \
  && curl -OL $PANDOC_URL \
  && dpkg -i *.deb \
  && rm *.deb 

# crossref
RUN CROSSREF_TAR="pandoc-crossref-Linux-X64.tar.xz" \
  &&curl -OL https://github.com/lierdakil/pandoc-crossref/releases/latest/download/$CROSSREF_TAR \
  && tar -xf $CROSSREF_TAR \
  && rm $CROSSREF_TAR \
  && ls \
  && mv pandoc-crossref /usr/local/bin/ \
  && chmod a+x /usr/local/bin/pandoc-crossref \
  && mkdir -p /usr/local/man/man1 \
  && mv pandoc-crossref.1 /usr/local/man/man1 

# cleanup
RUN rm -rf /var/lib/apt/lists/*

# install pandemics
# ==========================================================================
# setup ssh to allow recipe downloads
RUN mkdir -p $HOME/.ssh
RUN echo "Host *\n\tStrictHostKeyChecking no\n" >> $HOME/.ssh/config

# Install pandemics (PANDEMICS_DEPS=FALSE not working!?)
#RUN npm install --global --ignore-scripts --omit-dev --omit-optional pandemics@$PANDEMICS_VERSION 
RUN mkdir /app && cd /app \
  && PANDEMICS_DEPS=FALSE npm install --foreground-script --omit=optional --omit=dev pandemics@$PANDEMICS_VERSION \
  && ln -s /app/node_modules/.bin/* /usr/local/bin

# get ready
# ==========================================================================
RUN mkdir -p /data
WORKDIR /data
ENTRYPOINT ["pandemics"]
