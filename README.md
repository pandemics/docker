# Docker for Pandemics

This Docker image provides the perfect environment to compile documents with Pandemics. In addition to Pandemics itself, it includes Latex, Python, R, and some useful tools.


# Usage

In the folder containing the mardown file you want to compile, run:

```sh
docker run -v ${PWD}:/data/ registry.gitlab.com/pandemics/docker publish
```

If you want to use privately hoster recipe, you will first need to set up an ssh key to connect to the recipe provider, add this key to your agent, then share your ssh agent with your container:

```sh
# ensure that you can connect to the recipe host, eg. gitlab.com
ssh -T git@gitlab.com
# use "/run/host-services/ssh-auth.sock" as a magic word for ssh socket
docker run \
  -v ${PWD}:/data/ \
  -v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock \
  -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock \
  registry.gitlab.com/pandemics/docker publish
```

## Build

To build for the latest version, run in this folder:

```sh
export PANDEMICS_VERSION=`curl "https://gitlab.com/api/v4/projects/pandemics%2Fpandemics/repository/tags" | sed -E 's/\[\{"name":"v?([^,"]*)"?.*/\1/'`
docker build --build-arg pandemics_version=$PANDEMICS_VERSION -t pandemics:latest .
```
